import {Routes, Route} from 'react-router-dom';

import Home_page from './pages/home_page';
import List_candidate from './pages/list_candidate';
import Dashboard from './pages/dashboard';
import Create_Candidate_page from './pages/create_candidate';
import Lk_candidate from './pages/lk_candidatePage';

import './App.css';




function App() {
  return (
    <div className="App">
      <Routes>
        <Route path ='/' element = {<Home_page/>}/>
        <Route path ='/list_candidate' element = {<List_candidate/>}/>
        <Route path ='/dashboard' element = {<Dashboard/>}/>
        <Route exact path='/create_candidate' element = {<Create_Candidate_page/>}/>
        <Route exact path='/lk_candidate' element = {<Lk_candidate/>}/>
      </Routes>
    </div>
        
  )
}

export default App;
