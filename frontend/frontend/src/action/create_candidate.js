import axios from 'axios';
export const create_candidate = async (name, first_name,age,university,
                                       work_experience, 
                                       programming_language, 
                                       number_of_scientific_articles) => {
    try{

    const response = await axios.post('http://localhost:8000/api/candidate/', {
        name,
        first_name,
        age,
        university,
        work_experience,
        programming_language,
        number_of_scientific_articles
    })
    alert(response.data.message);
    } catch(e){
        alert(e)

    }


}