// rafce
import React, { useState } from 'react'
import {Link } from 'react-router-dom';

import Input from '../components/input'
import Footer from '../components/footer';
import Header_component from '../components/header_component';
import '../assets/css/create_candidate.css'
import { create_candidate } from '../action/create_candidate';


const useCreate_candidate = () => {
    const [name, setName] = useState ('')
    const [first_name, setFirst_name] = useState ('')
    const [age, setAge] = useState ('')
    const [university, setUniversity] = useState ('')
    const [work_experience, setWork_experience] = useState ('')
    const [programming_language, setProgramming_language] = useState ([])
    const [number_of_scientific_articles, setNumber_of_scientific_articles] = useState ('')

  return (
    <div className="page-container">
            <Header_component />   

            <div className="content-wrap">
                <div className='table_add_condidate'>
                    <Input value = {name} setValue = {setName} type = 'text' placeholder = 'name'/>
                    <Input value = {first_name} setValue = {setFirst_name} type = 'text' placeholder = 'first_name'/>
                    <Input value = {age} setValue = {setAge} type = 'text' placeholder = 'age'/>
                    <Input value = {university} setValue = {setUniversity} type = 'text' placeholder = 'university'/>
                    <Input value = {work_experience} setValue = {setWork_experience} type = 'text' placeholder = 'work_experience'/>
                    <Input value = {programming_language} setValue = {setProgramming_language} type = 'text' placeholder = 'programming_language'/>
                    <Input value = {number_of_scientific_articles} setValue = {setNumber_of_scientific_articles} type = 'text' placeholder = 'number_of_scientific_articles'/>
                </div>
                <Link to='/list_candidate'>
                  <button className='add_candidate_button' onClick={() => create_candidate(name,
                                                                                          first_name,
                                                                                          age,
                                                                                          university,
                                                                                          work_experience,
                                                                                          [programming_language],
                                                                                          number_of_scientific_articles,)
                    }>Добавить кандидата </button>
                </Link>
              



            </div> 
            <Footer /> 
        </div>
  )
}

export default useCreate_candidate
