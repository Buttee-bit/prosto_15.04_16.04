import React,{useEffect, useState} from 'react';
import card_candidate from '../components/card_candidate'
import Footer from '../components/footer';
import Header_component from '../components/header_component';


const List_candidate = () =>{
    const [candidate, setCandidate] = useState([])
    
    useEffect(() => {
    fetch('http://localhost:8000/api/candidate/')
        .then(response => response.json())
        .then(data => setCandidate(data));
    }, [])

    return (
        <div className="page-container">       
            <Header_component />
            <div className="content-wrap">   
                {candidate.map(card_candidate)}
            </div> 
            <Footer /> 
        </div>

    )
}

export default List_candidate;