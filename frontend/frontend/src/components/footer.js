import React from "react";
import '../assets/css/footer.css';
const Footer = () =>{

    return (
        <div className="main-footer">
            <div className="container">
                <div className="row">
                    {/* {Column 1} */}
                    <div className="col">
                        <a>Команда SoL</a>
                    </div>
                    {/* {Column 2} */}
                    <div className="col">
                        <a>Связаться с нами </a>
                        
                    </div>
                    {/* {Column 3} */}
                    <div className="col">
                        <a href='https://gitlab.com/Buttee-bit/prosto_15.04_16.04'>Ссылка на GitLab</a>
                    </div>

                </div>
                <hr />
                <div className="row">
                    <p className="col-sm">
                        &copy;{new Date().getFullYear()} Просто x Сбербанк | SoL

                    </p>

                </div>
            </div>
        </div>
    )
}

export default Footer;