const FeedbackCard = ({ name, first_name, age, university, work_experience, programming_language, number_of_scientific_articles }) => {

    return (
      <div className="main">
        <div className="main_2">
          <div className="main_3">
            <h4 className="main_4">
              <div>Имя : {name}</div>
              <div>Фамилия : {first_name}</div>
              <div>Возраст : {age}</div>
              <div>Университет: {university}</div>
              <div>Опыт работы: {work_experience}</div>
              <div>Языки программирования: {programming_language}</div>
              <div>Количество науных статей: {number_of_scientific_articles}</div>

            </h4>
            <p className="main_5">
              {name}
            </p>
          </div>
        </div>
      </div>
    )
  };

export default FeedbackCard;
