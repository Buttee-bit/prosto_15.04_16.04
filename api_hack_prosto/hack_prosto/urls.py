from django.urls import path
from django.views.generic import TemplateView

app_name  = 'hack_prosto'

urlpatterns = [
    path('', TemplateView.as_view(template_name = 'hack_prosto/index.html')),
]
