from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class University(models.Model):
    name = models.CharField(max_length=128)
    power = models.IntegerField(default=10)

    def __str__(self):
        return self.name


class Programming_language(models.Model):
    name = models.CharField(max_length=128)
    power = models.IntegerField(default=10)

    def __str__(self):
        return self.name


class Candidate(models.Model):

    name = models.CharField(max_length=128)
    
    first_name  = models.CharField(max_length=128)
    age = models.IntegerField()
    
    university = models.ForeignKey(
        University, on_delete=models.PROTECT)
    work_experience = models.IntegerField(default=1)
    
    programming_language = models.ManyToManyField(
        Programming_language)
    number_of_scientific_articles = models.IntegerField(default=0)
    score = models.IntegerField(default=1000)
    
    class Meta:
        ordering = ('score',)

