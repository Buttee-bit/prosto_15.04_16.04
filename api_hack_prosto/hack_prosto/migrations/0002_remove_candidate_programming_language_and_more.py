# Generated by Django 4.1.1 on 2023-04-02 10:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hack_prosto', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='candidate',
            name='programming_language',
        ),
        migrations.AddField(
            model_name='candidate',
            name='programming_language',
            field=models.ManyToManyField(to='hack_prosto.programming_language'),
        ),
    ]
