from rest_framework import serializers
from hack_prosto.models import University, Candidate, Programming_language


class CandidateSerializers(serializers.ModelSerializer):
    class Meta:
        model = Candidate
        fields = ('id', 'name', 'first_name', 'age',
                  'university', 'work_experience',
                  'programming_language',
                  'number_of_scientific_articles', 'score')


class CreateCandidateSerializers(serializers.ModelSerializer):
    class Meta:
        model = Candidate
        ields = ('id', 'name', 'first_name', 'age',
                  'university', 'work_experience',
                  'programming_language',
                  'number_of_scientific_articles')


class UniversitySerializers(serializers.ModelSerializer):
    class Meta:
        model = University
        fields = ('id', 'name', 'power',)


class Programming_LanguageSerializers(serializers.ModelSerializer):
    class Meta:
        model = Programming_language
        fields = ('id', 'name', 'power',)
