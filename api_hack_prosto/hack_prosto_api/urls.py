from django.urls import path
from hack_prosto.models import *
from rest_framework.routers import DefaultRouter
from .views import Candidatelist, UniversityList, Programming_LanguageList


# router = DefaultRouter()
# router.register(r'candidate', Candidatelist,  basename='candidate')
# urlpatterns = router.urls

app_name = 'hack_prosto_api'

urlpatterns = [
    path('candidate/', Candidatelist.as_view(), name='detailcreate'),
    path('candidate/<int:pk>/', Candidatelist.as_view(), name='listcreate'),
    path('univsrsity/', UniversityList.as_view(), name='university_list'),
    path('programminglanguage/', Programming_LanguageList.as_view(), name='candidate_list'),
]
