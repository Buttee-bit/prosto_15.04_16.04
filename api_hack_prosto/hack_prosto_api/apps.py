from django.apps import AppConfig


class HackProstoApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hack_prosto_api'
