from django.shortcuts import get_object_or_404
from requests import Response
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework import status

from hack_prosto.models import *
from .serializers import CandidateSerializers, Programming_LanguageSerializers, UniversitySerializers, CreateCandidateSerializers
class Candidatelist(generics.ListCreateAPIView):
    queryset = Candidate.objects.all()
    serializer_class = CandidateSerializers


class CreateCandidate(APIView):
    def post(self, request):
        output = [
            {
                ''
            }
        ]
        pass
    
    def get(self, request):
        pass
    
    
class Candidatedetail(generics.RetrieveDestroyAPIView):
    pass


class Programming_LanguageList(generics.ListCreateAPIView):
    queryset = Programming_language.objects.all()
    serializer_class = Programming_LanguageSerializers
    
class UniversityList(generics.ListCreateAPIView):
    queryset = University.objects.all()
    serializer_class = UniversitySerializers

